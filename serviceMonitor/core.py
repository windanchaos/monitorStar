import requests
import socket
def checkGetHttp(url):
    response=requests.get(url)
    return response.status_code == 200

def checkPostHttp(url,data):
    if(type(data) is not dict):
        raise "post data should be dict"
    response=requests.post(url,data=data)
    return response.status_code == 200

def checkSocket(ip, port):
    # 参数处理，支持文本
    port=int(port)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = s.connect_ex((ip,port))
    if result == 0:
        return True
    else:
        return False
